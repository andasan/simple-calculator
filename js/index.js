let newEquation = true
let doubleOP = false
let doubleMinus = false
let solved = false

function addVal(val) 
{ 
    if(solved){ 
        clr()
        solved = false
        newEquation = true
        doubleMinus = false
    }

    if(newEquation && newEquationOp(val)){
    }else{
        newEquation = false
    
        let x = document.getElementById("result").value
        console.log(x)
        console.log(typeof(x))
        console.log(val)
        console.log(typeof(val))

        if(x.length > 0){
            let previousVal = x[x.length-1]
            console.log(previousVal)

            if(checkifOp(previousVal) && checkifOp(val)){
                doubleOP = true
                console.log("double trouble")
                if((previousVal === "+" || previousVal === "*" ||previousVal === "/") && val === "-" && !doubleMinus){
                    console.log("This is a minus sign it could still be used")
                    document.getElementById("result").value+=val 
                }
                if(previousVal === "-" && val === "-"){
                    console.log("convert -- to +")

                    var tobeSliced = document.getElementById("result").value
                    var Result = tobeSliced.slice(0,-1);
                    Result+="+"

                    //clear the values and replace with sliced result
                    clr()
                    
                    console.log("what is x now? === " +Result)
                    document.getElementById("result").value+=Result
                    doubleMinus = true
                }
            }else{
                doubleOP = false
            }

            if(!doubleOP){
                document.getElementById("result").value+=val 
            }
        }else{
            document.getElementById("result").value+=val 
        }   
    }
} 

function solve() { 
    let x = document.getElementById("result").value 
    let y = eval(x) 
    document.getElementById("result").value = y 
    solved = true
} 
  
function clr() { 
    document.getElementById("result").value = "" 
} 

function newEquationOp(val){
    if(val == "+" || val == "*" || val == "/" ){
        return true
    }
    return false
}

function checkifOp(val){
    if(val == "+" || val == "-" || val == "*" || val == "/" ){
        return true
    }
    return false
}

//just in case there's a use for this
function pos_to_neg(num){
    return -Math.abs(num);
}
