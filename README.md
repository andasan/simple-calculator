# Simple Calculator

A simple calculator built in HTML, styled with CSS and programmed with Javascript


## [DEMO](https://laughing-benz-128cff.netlify.com/)


# Preview

![alt text](https://gitlab.com/andasan/simple-calculator/raw/master/img/ss2.png "Calculator")
